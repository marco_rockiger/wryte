(ns wryte.core
  (:require [figwheel.client :as fw :include-macros true]
            [reagent.core :as reagent :refer [atom]]
            [wryte.views :as views]
            [wryte.handlers :as handlers]))

(enable-console-print!)

(fw/watch-and-reload
  :websocket-url   "ws://localhost:3449/figwheel-ws"
  :jsload-callback 'mount-root)

(enable-console-print!)

(defn mount-root []
  (reagent/render [views/root-component]
                  (.getElementById js/document "app")))

(defn ^:export init! []
  (println "startet")
  (handlers/setup-webspeech)
  (mount-root)
  (handlers/setup-editor)
  (handlers/register-keyevents)
  (handlers/register-mousevents))

(init!)
