(ns wryte.views
  (:require-macros [hiccups.core :as hiccups :refer [html]])
  (:require [wryte.subs :as s]
            [wryte.handlers :as h]
            [wryte.db :as db]
            [cljs.test :refer-macros [is deftest]]
            [clojure.string :refer [split capitalize]]
            [cljs.nodejs :as nj]
            [reagent.core :as reagent]
            [cljs.nodejs :as nodejs]
            [hickory.render :as render]
            [hiccups.runtime :as hiccupsrt]))

(defn log [x] (.log js/console x))
;;========
;; Helper:
(defn linebreak
  "String -> ListOfParagraph
  consumes a string, splits the string \\n and produces a list of hicup paragraphs"
  [s]
  (let [two-line #"\n\n"
        one-line #"\n+"
        lop (split s one-line)]
     (for [p lop]
      [:p p])))

(is (= [[:p "String1"] [:p "String2"]]
       (linebreak "String1\nString2")))

(defn button [id class on-click text]
  [:button {:id id :class class :on-click on-click}
    text])

(defn start-button []
  (let [btn-class (if (s/recording?) " btn-negative" " btn-positive")]
   (button :start_button (str "btn btn-large" btn-class) h/on-click-start-button
    (if-not (s/recording?)
       "Start"
       "Stop"))))

(defn colon-button []
  (button :colon_button "btn btn-large btn-default punc_button" h/on-click-colon "."))

(defn comma-button []
  (button :comma_button "btn btn-large btn-default punc_button" h/on-click-comma ","))

(defn dquote-button []
  (button :dquote_button "btn btn-large btn-default punc_button" h/on-click-dquote "\""))

(defn squote-button []
  (button :squote_button "btn btn-large btn-default punc_button" h/on-click-squote "'"))

(defn pgraph-button []
  (button :pgraph_button "btn btn-large btn-default punc_button" h/on-click-pgraph "¶"))

(defn dictator-component []
  (let [display (if (not (db/edit?)) :flex :none)]
    [:div {:id :dictator_component :style {:display display}}
      [:pre {:id :dictator :style {:margin :0 :-webkit-user-select :text}} (s/fresult)]
      [:footer {:id :toolbar :class "toolbar toolbar-footer"}
        [:span {:id :interim_result :style {:color :grey :flex-grow 1}} (db/iresult)]
        (pgraph-button)
        (squote-button)
        (dquote-button)
        (comma-button)
        (colon-button)
        (start-button)]]))

(defn editor-component []
  (let [display (if (db/edit?) :flex :none)
        test (println (html (s/fresult)))]
    [:div {:id :editor-wrapper :style {:display display}}
      [:pre {:id :editor :value (s/fresult) :on-blur h/on-blur-editor
             :content-editable :true :read-only (not (db/edit?))
             :dangerouslySetInnerHTML {:__html (html (s/fresult))}}]]))
             ;; TODO react on content change in editor.



(defn view-switcher []  ;; TODO:0 Switch of view based on db/editor
  (let [dict_active (if (db/edit?) "" " active")
        edit_active (if (db/edit?) " active" "")]
   [:div {:id :view-switcher :class :toolbar-actions}
     [:div {:class :btn-group}
       [:button {:id :dict :class (str "btn btn-large btn-default" dict_active) :on-click h/on-click-dict-button}
         [:span {:class "icon icon-mic"}] "Dictate"]
       [:button {:id :edit :class (str "btn btn-large btn-default" edit_active) :on-click h/on-click-edit-button}
         [:span {:class "icon icon-pencil"}] "Edit"]]]))

(defn debug-info []
  [:div
    [:span ":punc " (db/punc)] " | "
    [:span ":checked? " (str (db/checked?))] " | "
    [:span ":pos " (db/pos)] " | "
    [:span ":edit? " (str (db/edit?)) " | "]
    [:span ":language " (str (db/language)) " | "]
    [:span ":recording? " (str (db/recording?))]
    [:div ":ignore " (str (db/ignore-list))]
    [:div ":iresult " (db/iresult)]
    [:div ":fresult " (str (db/fresult))]])

(defn root-component []
  [:div {:id :root :class :window}
    [:header {:class "toolbar toolbar-header"}
      (view-switcher)
      #_
      [:button {:id :language-chooser :class "btn btn-default btn-dropdown pull-right"}
        "Deutsch" [:span {:class "icon icon-down-open right"}]]
      [:select {:id :language-chooser :value (db/language)
                :class "btn btn-default btn-dropdown pull-right"
                :on-change h/on-change-language-chooser
                :disabled (db/recording?)}
        (for [lang (keys db/languages)]
          [:option {:value lang :on-click #(h/on-click-dropdown lang)}
            (-> lang name capitalize)])]]
    [:div {:class :window-content}
        (editor-component)
        (dictator-component)]
    (if false (debug-info))])
