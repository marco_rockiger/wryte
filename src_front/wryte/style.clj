(ns akiee.style
  (:require [garden.core :refer [css]]
            [garden.units :as u :refer [px pt percent]]
            [garden.selectors :refer [nth-of-type]]
            [garden.color :refer [lighten darken]]))

;; Basic stylesheet for akiee

;; =================
;; Helpers:


;; =================
;; Constants:

(def variant "light")

(def border-color "#ccc")
(def toolbar-border (str "1px solid" border-color))

;; When color definition differs for dark and light variant,)
;; it gets @if ed depending on $variant)


(def base_color (if (= variant "light") "#ffffff" "#404552"))
(def text_color (if (= variant "light") "#5c616c" "#D3DAE3"))
(def bg_color (if (= variant "light") "#F5F6F7" "#383C4A"))
(def fg_color (if (= variant "light") "#5c616c" "#D3DAE3"))

(def selected_fg_color "#ffffff")
(def selected_bg_color "#5294E2")

(def selected_borders_color (darken selected_bg_color 20))
(def borders_color (if (= variant "light")
                       (darken bg_color 9)
                       (darken bg_color 6)))
;
; $link_color: if($variant == 'light', darken($selected_bg_color,10%),
;                                      lighten($selected_bg_color,20%));
; $link_visited_color: if($variant == 'light', darken($selected_bg_color,20%),
;                                      lighten($selected_bg_color,10%));
;
; $selection_mode_bg: if($transparency == 'true', transparentize($selected_bg_color, 0.05), $selected_bg_color);
; $warning_color: #F27835;
; $error_color: #FC4138;
(def success_color "#73d216");
(def positive_bg_color (darken success_color 9))
(def positive_sl_bg_color success_color)

(def destructive_color "#F04A50")
(def negative_bg_color destructive_color)
(def negative_sl_bg_color (darken negative_bg_color 10))
; $suggested_color: #4DADD4;
;
; //insensitive state derived colors
; $insensitive_fg_color: if($variant == 'light', transparentize($fg_color, 0.45), transparentize($fg_color, 0.55));
; $insensitive_bg_color: if($variant == 'light', mix($bg_color, $base_color, 40%), lighten($bg_color, 2%));
;
; $header_bg: red;
; @if $transparency=='true' and $variant=='light' { $header_bg: transparentize(#e7e8eb, 0.05)}; }
; @if $transparency=='false' and $variant=='light' { $header_bg: #e7e8eb}; }
; @if $transparency=='true' and ($variant=='dark' or $darker=='true') { $header_bg: transparentize(#2f343f, 0.03)}; }
; @if $transparency=='false' and ($variant=='dark' or $darker=='true') { $header_bg: #2f343f}; }
;
; $header_bg_backdrop: if($darker == 'true' or $variant == 'dark', lighten($header_bg, 1.5%), lighten($header_bg, 3%));
;
; $header_border: if($variant == 'light' and $darker=='false', darken($header_bg, 7%), darken($header_bg, 4%));
;
; $header_fg: if($variant == 'light', saturate(transparentize($fg_color, 0.2), 10%), saturate(transparentize($fg_color, 0.2), 10%));
; $header_fg: if($darker == 'true', saturate(transparentize(#D3DAE3, 0.2), 10%), $header_fg);
;
; $dark_sidebar_bg: if($transparency == 'true', transparentize(#353945, 0.05), #353945);
; $dark_sidebar_fg: #BAC3CF;
; $dark_sidebar_border: if($variant == 'light', $dark_sidebar_bg, darken($dark_sidebar_bg, 5%));
;
; $osd_fg_color: $dark_sidebar_fg;
; $osd_bg_color: $dark_sidebar_bg;
;
; $osd_button_bg: transparentize(lighten($osd_bg_color, 22%), 0.6);
; $osd_button_border: transparentize(darken($osd_bg_color, 12%), 0.6);
;
; $osd_entry_bg: transparentize(lighten($osd_bg_color, 22%), 0.6);
; $osd_entry_border: transparentize(darken($osd_bg_color, 12%), 0.6);
;
; $osd_insensitive_bg_color: darken($osd_bg_color, 3%);
; $osd_insensitive_fg_color: mix($osd_fg_color, opacify($osd_bg_color, 1), 30%);
; $osd_borders_color: transparentize(black, 0.3);
;
; $panel_bg: darken($dark_sidebar_bg, 4.7%);
; $panel_fg: $dark_sidebar_fg;
;
; $entry_bg: if($variant=='light', $base_color, lighten($base_color, 0%));
; $entry_border: if($variant == 'light', #cfd6e6, darken($borders_color, 0%));
;
; $header_entry_bg: if($darker == 'true' or $variant == 'dark', transparentize(lighten($header_bg, 22%), 0.6), transparentize($base_color, 0.1));
; $header_entry_border: if($darker == 'true' or $variant == 'dark', transparentize(darken($header_bg, 12%), 0.6), transparentize($header_fg, 0.7));
;
; $button_bg: if($variant == 'light', lighten($bg_color, 2%), lighten($base_color, 2%));
; $button_border: $entry_border;
;
; $header_button_bg: if($darker == 'true' or $variant == 'dark', transparentize(lighten($header_bg, 22%), 0.6), transparentize($button_bg, 0.1));
; $header_button_border: if($darker == 'true' or $variant == 'dark', transparentize(darken($header_bg, 12%), 0.6), transparentize($header_fg, 0.7));
;
; //WM Buttons
;
; // Close
; $wm_button_close_bg: if($variant == 'light' and $darker == 'false', #f46067, #cc575d);
; $wm_button_close_hover_bg: if($variant == 'light' and $darker == 'false', #f68086, #d7787d);
; $wm_button_close_active_bg: if($variant == 'light' and $darker == 'false', #f13039, #be3841);
;
; $wm_icon_close_bg: if($variant == 'light' and $darker == 'false',#F8F8F9 , #2f343f);
;
; // Minimize, Maximize
; $wm_button_hover_bg: if($variant == 'light' and $darker == 'false', #fdfdfd, #454C5C);
; $wm_button_active_bg: $selected_bg_color;
;
; $wm_button_hover_border: if($variant == 'light' and $darker == 'false', #D1D3DA, #262932);
;
; $wm_icon_bg: if($variant == 'light' and $darker == 'false', #90949E, #90939B);
; $wm_icon_unfocused_bg: if($variant == 'light' and $darker == 'false', #B6B8C0, #666A74);
; $wm_icon_hover_bg: if($variant == 'light' and $darker == 'false', #7A7F8B, #C4C7CC);
; $wm_icon_active_bg: $selected_fg_color;

;; =================
;; Style:

(css {:output-to "app/css/style.css"}
  [:#root {:display :flex :flex-direction :column :height :100vh}]
  [:#dictator_component {:display :flex :flex-direction :column :width :100vw}
    [:div {:padding :10px :cursor :text}
      [:p :span {:-webkit-user-select :text :cursor :text}]]]
  [:#dictator :#editor {:display :flex :flex-direction :column :flex-grow 1 :padding :10px :font-family :monospace :-webkit-user-select :text :cursor :text :white-space :normal}]
  [:#editor [:p {:min-height :1.6em :min-width :1.6em}]]
  [:#toolbar {:padding :8px :display :flex :justify-content :flex-end
              :height :47px :min-height :47px}]
  [:header.toolbar-header {:display :flex}]
  [:.punc_button {:margin-right :5px}]
  [:#view-switcher {:display :flex :justify-content :center :padding-top :3px :height :39px :flex-grow 1}
    [:button {:width :100px}]]
  [:#language-chooser {:margin-top :7px :margin-bottom :6px :margin-right :10px :padding-left :12px}]
  [:.btn-dropdown {:width :100px :text-align :left}]
  [:.btn-dropdown:after {:content :none}]
  [:.right {:float :right}]
  [:.btn [:.icon.right {:float :right}]]
  [:select:disabled {:color border-color}]

  ;; Theme overides
  [:body {:margin 0 :font-size :14px :font-family "'Source Sans Pro',system, -apple-system, '.SFNSDisplay-Regular', 'Helvetica Neue', Helvetica, 'Segoe UI', sans-serif"}]
  [:pre :textarea {:font-family "'Soure Code Pro', monospace"}]
  [:.toolbar {:background-color bg_color :background-image :none}]
  [:.toolbar-header {:border-color borders_color}]
  [:.btn-group [:.active {:background-color selected_bg_color}]]
  [:.btn-default {:background-color bg_color :background-image :none :border-color borders_color}]
  [:.btn {:box-shadow :none :font-size :14px}]
  [:.btn-default:active {:background-color selected_bg_color :color selected_fg_color} [:.icon {:color selected_fg_color}]]
  [:.btn-positive {:border-color positive_bg_color :background-color positive_bg_color :background-image :none}]
  [:.btn-positive:active {:border-color positive_sl_bg_color :background-color positive_sl_bg_color :background-image :none}]
  [:.btn-negative {:border-color destructive_color :background-color destructive_color :background-image :none}]
  [:.btn-negative:active {:border-color negative_sl_bg_color :background-color negative_sl_bg_color :background-image :none}]
  [:.error {:text-decoration :underline :color :red :cursor "pointer !important"}]
  [:.ignored {:text-decoration :underline :color :green :cursor "pointer !important"}])
