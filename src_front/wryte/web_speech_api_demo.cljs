(ns wryte.core
  (:require [figwheel.client :as fw :include-macros true]
            [reagent.core :as reagent :refer [atom]]
            [cljs.pprint :as pprint :refer [pprint]]))

(enable-console-print!)

(fw/watch-and-reload
  :websocket-url   "ws://localhost:3449/figwheel-ws"
  :jsload-callback 'mount-root)

(enable-console-print!)

(defonce state (atom {:message "Hello Reagent world"
                      :create_email false
                      :final_transcript ""
                      :recognizing false
                      :ignore_onend false
                      :start_timestamp nil
                      :recognition nil}))

(def langs  [["Afrikaans",       ["af-ZA"]]
             ["Bahasa Indonesia",["id-ID"]],
             ["Bahasa Melayu",   ["ms-MY"]],
             ["Català",          ["ca-ES"]],
             ["Čeština",         ["cs-CZ"]],
             ["Deutsch",         ["de-DE"]],
             ["English",         ["en-AU", "Australia"],
                                 ["en-CA", "Canada"],
                                 ["en-IN", "India"],
                                 ["en-NZ", "New Zealand"],
                                 ["en-ZA", "South Africa"],
                                 ["en-GB", "United Kingdom"],
                                 ["en-US", "United States"]],
             ["Español",         ["es-AR", "Argentina"],
                                 ["es-BO", "Bolivia"],
                                 ["es-CL", "Chile"],
                                 ["es-CO", "Colombia"],
                                 ["es-CR", "Costa Rica"],
                                 ["es-EC", "Ecuador"],
                                 ["es-SV", "El Salvador"],
                                 ["es-ES", "España"],
                                 ["es-US", "Estados Unidos"],
                                 ["es-GT", "Guatemala"],
                                 ["es-HN", "Honduras"],
                                 ["es-MX", "México"],
                                 ["es-NI", "Nicaragua"],
                                 ["es-PA", "Panamá"],
                                 ["es-PY", "Paraguay"],
                                 ["es-PE", "Perú"],
                                 ["es-PR", "Puerto Rico"],
                                 ["es-DO", "República Dominicana"],
                                 ["es-UY", "Uruguay"],
                                 ["es-VE", "Venezuela"]],
             ["Euskara",         ["eu-ES"]],
             ["Français",        ["fr-FR"]],
             ["Galego",          ["gl-ES"]],
             ["Hrvatski",        ["hr_HR"]],
             ["IsiZulu",         ["zu-ZA"]],
             ["Íslenska",        ["is-IS"]],
             ["Italiano",        ["it-IT", "Italia"],
                                 ["it-CH", "Svizzera"]],
             ["Magyar",          ["hu-HU"]],
             ["Nederlands",      ["nl-NL"]],
             ["Norsk bokmål",    ["nb-NO"]],
             ["Polski",          ["pl-PL"]],
             ["Português",       ["pt-BR", "Brasil"],
                                 ["pt-PT", "Portugal"]],
             ["Română",          ["ro-RO"]],
             ["Slovenčina",      ["sk-SK"]],
             ["Suomi",           ["fi-FI"]],
             ["Svenska",         ["sv-SE"]],
             ["Türkçe",          ["tr-TR"]],
             ["български",       ["bg-BG"]],
             ["Pусский",         ["ru-RU"]],
             ["Српски",          ["sr-RS"]],
            ;; Asian languages are commentet out
             ; ["한국어",            ["ko-KR"]],
             ; ["中文",             ["cmn-Hans-CN", "普通话 (中国大陆)",
             ;                     ["cmn-Hans-HK", "普通话 (香港)"],
             ;                     ["cmn-Hant-TW", "中文 (台灣)"],
             ;                     ["yue-Hant-HK", "粵語 (香港)"]]],
             ; ["日本語",           ["ja-JP"]],
             ["Lingua latīna",   ["la"]]])

(defn start-button [ev]
  (do
    (println "Start Button pressed")
    (if (:recognizing @state)
      (.stop (:recognition @state))
      (do
        (.start (:recognition @state))
        (println "Es geht los")))))


(defn root-component []
  [:div
    [:style "
  * {
    font-family: Verdana, Arial, sans-serif;
  }
  a:link {
    color:#000;
    text-decoration: none;
  }
  a:visited {
    color:#000;
  }
  a:hover {
    color:#33F;
  }
  .button {
    background: -webkit-linear-gradient(top,#008dfd 0,#0370ea 100%);
    border: 1px solid #076bd2;
    border-radius: 3px;
    color: #fff;
    display: none;
    font-size: 13px;
    font-weight: bold;
    line-height: 1.3;
    padding: 8px 25px;
    text-align: center;
    text-shadow: 1px 1px 1px #076bd2;
    letter-spacing: normal;
  }
  .center {
    padding: 10px;
    text-align: center;
  }
  .final {
    color: black;
    padding-right: 3px;
  }
  .interim {
    color: gray;
  }
  .info {
    font-size: 14px;
    text-align: center;
    color: #777;
    display: none;
  }
  .right {
    float: right;
  }
  .sidebyside {
    display: inline-block;
    width: 45%;
    min-height: 40px;
    text-align: left;
    vertical-align: top;
  }
  #headline {
    font-size: 40px;
    font-weight: 300;
  }
  #info {
    font-size: 20px;
    text-align: center;
    color: #777;
    visibility: hidden;
  }
  #results {
    font-size: 14px;
    font-weight: bold;
    border: 1px solid #ddd;
    padding: 15px;
    text-align: left;
    min-height: 150px;
  }
  #start_button {
    border: 0;
    background-color:transparent;
    padding: 0;
  }"]
    [:h1 {:class "center" :id "headline"} "wryte"]
    [:div
      [:p {:id "info_start"} "Click on the microphone and begin speaking."]
      [:p {:id "info_speak_now"} "Speak now."]
      [:p {:id "info_no_speech"} "No speech was detected. You may need to addjust your " [:a {:href "//support.google.com/chrome/bin/answer.py?hl=en&amp;answer=1407892"} "microphone settings"] "."]
      [:p {:id "info_no_microphone" :style {:display :none}}  "No microphone was found. Ensure that a microphone is installed and that " [:a  {:href "//support.google.com/chrome/bin/answer.py?hl=en&amp;answer=1407892"} "microphone settings"] "are configured correctly."]
      [:p {:id "info_allow"} "Click the \"Allow\" button  above to enable your microphone"]
      [:p {:id "info_denied"} "Permission to use microphone was denied."]
      [:p {:id "info_blocked"} "Permission to use microphone is blocked. To change, go to chrome://settings/contentExceptions#media-stream"]
      [:p {:id "info_upgrade"} "Web Speech API is not supported by this browser.
     Upgrade to " [:a {:href "//www.google.com/chrome"} "Chrome  version 25 or later."]]]
   [:div {:class :right}
    [:button {:id :start_button :on-click start-button}
      [:img {:id :start_img :src "./img/mic.gif" :alt "Start"}]]]
   [:div {:class :results}
    [:span {:id :final_span :class :final}]
    [:span {:id :interim_span :class :interim}]
    [:p]]
   [:div {:class :center}
    [:div {:class :sidebyside :style {:text-align :right}}
      [:button {:id :copy_button :class :button :onclick "copyButton()"} "Copy and Paste"]
      [:div {:id :copy_info :class :info} "Press Control-C to copy text." [:br] "(Command-C on Mac.)"]]
    [:div {:class :sidebyside}
      [:button {:id :email_button :class :button :onclick "emailButton()"} "Create Email"]
      [:div {:id :email_info :class :info} "Text sent to default email application." [:br] "(See chrome://settings/handlers to change."]]
    [:p
      [:div {:id :div_language}
        [:select {:id :select_language :onchange "updateCountry()"}
         (for [l langs]
          [:option (first l)])]
        " "
        [:select {:id :select_dialect}]]]]])

(defn setup []
  (if (not (exists? (.-webkitSpeechRecognition js/window)))
    (println "pleas upgrde")
    (let [recognition (new js/window.webkitSpeechRecognition)]
      (println "Top")
      (swap! state assoc :recognition recognition)
      (set! (.-continuous recognition) true)
      (set! (.-interimResults recognition) true)
      (set! (.-onstart recognition)
        (fn []
          (do
            (swap! state assoc :recognizing true)
            (println "SPEAK NOW!"))))
      (set! (.-onerror recognition)
        (fn [event]
          (do
            (if (= (.-error event) "no-speech")
              (println "NO SPEECH")
              (swap! state assoc :ignore_onend true))
            (if (= (.-error event) "audio-capture")
              (println "NO MICROPHONE")
              (swap! state assoc :ignore_onend true))
            (if (= (.-error event) "not-allowed")
              (println "NOT ALLOWED")
              (swap! state assoc :ignore_onend true)))))
      (set! (.-onend recognition) (fn [] nil))
      (set! (.-onresult recognition)
        (fn [event]
          (let [interim_transcript (atom "")
                results (.-results event)
                length (.-length results)
                i (atom (.-resultIndex event))]
            (while (< @i length)
              (pprint (type results))
              (if (.-isFinal (aget results @i))
                (swap! state assoc :final_transcript (str (:final_transcript @state)(.-transcript (aget (aget results @i) 0))))
                (swap! state assoc :interim_transcript (str (:final_transcript @state)(.-transcript (aget (aget results @i) 0)))))
              (swap! i inc))
            (println "INTERIM " (:interim_transcript @state))
            (println "FINAL " (:final_transcript @state))))))))


(defn mount-root []
  (reagent/render [root-component]
                  (.getElementById js/document "app")))

(defn init! []
  (do
    (setup)
    (mount-root)))

(init!)
