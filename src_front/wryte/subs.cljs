(ns wryte.subs
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require [wryte.db :refer [app-db]]
            [wryte.speech-recognition :as sr]))

(enable-console-print!)

;; ========
;; Getters:

(defn fresult []
  (:fresult @app-db))

(defn iresult []
  (:iresult @app-db))

(defn recording? []
  (:recording? @app-db))
